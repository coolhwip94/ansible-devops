# Notes Ansible-Devops
> References to Ansible For Devops by Jeff Geerling
----------
## Playbooks
- Playbooks stored in playbooks directory
----------
## Ad-Hoc Commands
- Snippets for ad-hoc commands in ad-hoc.md
----------

## Hosts
> The hosts defined here can change basic structure needs to include "appserver" for app hosts, and a "db" host for database.
- ansible_user: defines user for ansible ssh connections
- ansible_become_pass: defines password for sudo, this can be encrypted, in this example it is not and uses `password`
- ansible_python_interpreter: defines default python interpreter for ansible, `hosts` files points to python3

----------

## Docker
> There is a Dockerfile to help set up environments to run ansible plays and playbooks against.
> docker-compose.yml will spin up 3 containers using the Dockerfile for the image.

- `docker-compose up -d`  will start up containers with exposed ports so you can ssh into the container remotely.

- Dockerfile
  - installs sudo, ssh
  - creates user appadmin, password = password
  - allows ssh on port 22
---
- > If you are on the same host that is running the containers, you can ssh directly into it by grabbing the ip and `ssh appadmin@<container_ip>`, password = `password`
- > Connecting from external hosts `ssh appadmin@<host_ip> -p <container_port>`
  - > example : `ssh appadmin@scripthost.hwip.lab -p 2021`


```
docker inspect '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' <container_name>
```
---
