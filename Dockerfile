FROM centos:7

RUN yum install openssh-server openssh-clients sudo -y
RUN sudo echo "Port 22" >> /etc/ssh/sshd_config
RUN systemctl enable sshd
RUN adduser appadmin
RUN usermod -aG wheel appadmin
RUN echo "appadmin:password" | chpasswd

ENTRYPOINT ["/usr/sbin/init", "-c"]

