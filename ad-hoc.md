# Ansible Ad Hoc Commands

- Space for Ansible Snippers pertaining to adhoc commands
- "mutli" refers to host group in this example


---
## Simple Info from Hosts

- Checking Disk utilization
    ```
    ansible multi -a "df -h"
    ```

- Checking hostname
    ```
    ansible multi -a "hostname"
    ```
    Single Thread
    ```
    ansible multi -a "hostname" -f 1
    ```

- Checking memory utilization
    ```
    ansible multi -a "free -m"
    ```


- Checking date
    ```
    ansible multi -a "date"
    ```

---

## Using Ansible Modules
---

-  Exhaustive List of all environment details, also known as "facts" in ansible lingo
    ```
    ansible multi -m setup
    ```
---
## Making changes

- Using yum to install
    ```
    ansible multi -b -K -m yum -a "name=ntp state=present"
    ```
> -b option is for "become" or running as sudo user
> -K is alias for --ask-become-pass, this is needed if the user needs to enter sudo pass to run sudo commands

- Installing pip
    ```
    ansible appserver -b -m yum -a "name=python3-pip state=present"
    ```

- Installing Django
    ```
    ansible appserver -b -m pip -a "name=django<4 state=present"
    ```

- Installing python3-setuptools
    ```
    ansible appserver -b -m yum -a "name=python3-setuptools state=present"
    ```
> Required before installing django

- Checking Django Version
    ```
    ansible appserver -a "python3 -m django --version"
    ```

- Configuring MariaDB and starting MariaDb
    ```
    ansible db -b -m yum -a "name=mariadb-server state=preset"
    ```
    ```
    ansible db -b -m service -a "name=mariadb state=started"
    ```

---

## Making changes to singular server

- Checking status of a service
  ```
  ansible appserver -b -a 'systemctl status ntpd'
  ```

- Restarting app on specific server
  ```
  ansible appserver -b -a 'service ntpd restart' --limit "<ip_address or host/hosts>"
  ```
> This example would not work if you utilized the docker-compose provided.

- Restarting app on specific server (alternate)
  ```
  ansible appserver1 -b -m service -a "name=ntpd state=restarted"
  ```
> Alternate method of restarting

---

## Managing users and groups
> Notice the usage of ansible.builtin. syntax, this is the updated way to refere to builtin modules
- Creating `admin` group
  ```
  ansible appserver -b -m ansible.builtin.group -a "name=admin state=present"
  ```

- Removing `admin` group
  ```
  ansible  appserver -b -m ansible.builtin.group -a "name=admin state=absent"
  ```

- Creating user with home directory
  ```
  ansible appserver -b -m ansible.builtin.user -a "name=johndoe group=admin createhome=yes"
  ```
  
  Create ssh key
  ```
  ansible appserver -b -m ansible.builtin.user -a "name=johndoe group=admin createhome=yes generate_ssh_key=yes"
  ```
  > Other parameters include
  > - UID : `uid=<uid>`
  > - shell : `shell=<shell>`
  > - password : `password=<password>`

- Removing User
  ```
  ansible appserver -b -m ansible.builtin.user -a "name=johndoe state=absent remove=yes"
  ```

> You can do pretty much everything you can do with `usermod` with ansible's user module

---

## Managing Packages
> Install and manage packages using yum, apt, or any other system specific package managers

- Using a generic package such as git
  ```
  ansible appserver -b -m package -a "name=git state=present"
  ```
---

## Manage Files and Directories
- Get information about a file using `stat` module
  ```
  ansible multi -m stat -a "path=/etc/environment"
  ```

- Copy a file to the servers
  ```
  ansible multi -m copy -a "src=/etc/hosts dest=/tmp/hosts"
  ```
> src can be file or directory, if you omit the trailing slash, the content AND directory will be copied.
> With trailing slash, only the contents are copied


- Retrieving a file from the servers
  ```
  ansible multi -b -m fetch -a "src=/etc/hosts dest=/tmp"
  ```
> This will place the files in /tmp/<hostname>/, unless you provide a `flat=yes` parameter, not recommended unless only copying from a single host.

- Creating Directories
  > Similar to `touch`
  ```
  ansible multi -m file -a "dest=/tmp/test mode=644 state=directory"
  ```

- Creating Symlink
  ```
  ansible multi -m file -a "src=/src/file dest=/dest/symlink state=link"
  ```

- Deleting Direcotries
  ```
  ansible multi -m file -a "dest=/tmp/test state=absent"
  ```
  

## Running Operations in the Background
> Useful for things like `yum update` or `apt update/upgrade` which can take a long time.

> `-B <seconds>`: maximum seconds to let the job run
> `-P <seconds>`: seconds to wait between polling servers for the updated job status

- Running yum update on all servers
  ```
  ansible multi -b -B 3600 -P 0 -a "yum -y update"
  ```
> Example of output, you can use the job id and `async_status` module to check on the job
```
appserver2 | CHANGED => {
    "ansible_job_id": "601193172210.5113",
    "changed": true,
    "finished": 0,
    "results_file": "/root/.ansible_async/601193172210.5113",
    "started": 1
}
appserver1 | CHANGED => {
    "ansible_job_id": "358630061656.5466",
    "changed": true,
    "finished": 0,
    "results_file": "/root/.ansible_async/358630061656.5466",
    "started": 1
}
```

- Using `async_status` to check the status of an asynchronous job.
  ```
  ansible multi -b -m async_status -a "jid=601193172210.5113"

  ```
> Playbooks can also utilize asynchronous operations by providing `async` and `poll` parameters.

## Checking Log Files
> The default ansible `command` module is not recommended for viewing large outputs, for example viewing logs where you would need to `tail -f`
> The `shell` module can be used in this case.

- Tailing logs
  ```
  ansible multi -b -m shell -a "tail /var/log/messages | grep ansible-command | wc -l"
  ```

## Managing cron jobs
> ansible assumes `*` if you do not provide a value, valid values are `day`, `hour`, `minute`, `month`, `weekday`.
> You can also specificy `user` parameter and `backup=yes` will backup the cron as well.

- Running shell script on a cron every day at 4 am
  ```
  ansible multi -b -m cron -a "name='daily-cron' hour=4 job='/path/to/daily-script.sh'
  ```

- Removing a Cron job
  ```
  ansible multi -b -m cron -a "name='daily-cron' state=absent"
  ```
---
## Deploying version controlled application (git)

- Deploying application using git
  ```
  ansible appserver -b git -a "repo=git://example.com/path/to/repo.git dest=/opt/myapp update=yes version=1.2.4"
  ```