# Ansible Data Manipulation

> https://docs.ansible.com/ansible/latest/user_guide/complex_data_manipulation.html

- `map`: this is a basic for loop that just allows you to change every item in a list, using the ‘attribute’ keyword you can do the transformation based on attributes of the list elements.


- `select/reject`: this is a for loop with a condition, that allows you to create a subset of a list that matches (or not) based on the result of the condition.


- `selectattr/rejectattr`: very similar to the above but it uses a specific attribute of the list elements for the conditional statement.


## Creating a dictionary from a list

```
 vars:
     single_list: [ 'a', 'b', 'c', 'd' ]
     mydict: "{{ dict(single_list | slice(2)) }}"

```



