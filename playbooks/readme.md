# Ansible Playbooks

Ansible Playbooks offer a repeatable, re-usable, simple configuration management and multi-machine deployment system, one that is well suited to deploying complex applications

For more information regarding playbooks visit [here](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html).

---

## Playbook Example for installing apache
- `install_apache.yml`
```
---
- hosts: all
  become: yes
  tasks:
    - name: install apache
      command: yum install --quiet -y httpd httpd-devel
    
    - name: copy configuration files
      command: >
        cp httpd.conf /etc/httpd/conf/httpd.conf
    - command: >
        cp httpd-vhosts.conf /etc/httpd/conf/httpd-vhosts.conf
    
    - name: start apache and configure it to run at boot
      command: service httpd start
    - command: chkconfig httpd on
```

- More playbook examples will be placed into `playbooks` directory.

---

## Running playbooks

- Limiting playbooks to particular host and groups
  ```
  ansible-playbook playbook.yml --limit <hosts groups or ip_address>
  ```

- View hosts that would be affected by playbook
  ```
  ansible-playbook playbook.yml --list-hosts
  ```

---

## Setting user and sudo options
- explicitly define remote user
  ```
  ansible-playbook playbook.yml --user=johndoe
  ```

  with sudo
  ```
  ansible-playbook playbook.yml --become --become-user=janedoe --ask-become-pass
  ```

  > Other options available `--inventory=PATH`, `--verbose`, `--extra-vars=VARS`, `--forks`, `--connection`, `--check`


---
## Playbook Examples

- Node Js example : `playbooks/nodejs_server_setup.yml`

  ```
  ansible-playbook playbooks/nodejs_server_setup.yml --extra-vars="node_apps_location=/usr/local/opt/node"
  ```

### Loading Variables from separate file
---

> This is just an example, environment is not setup to run this playbook as it uses ubuntu
- Ubuntu Lamp Server Example :`playbooks/ubuntu_lamp_server.yml`
- ansible allows you to run tasks before and after main tasks by defining `pre_tasks` and `post_tasks`
- `handlers` are special tasks run at the end of a play by adding the `notify` option to any of the tasks in that group.
  - only called at the end of a play
  - only called when tasks makes a change to the server and doesnt fail
  - to force handler to be called no matter what, add `--force-handlers` flag to ansible-playbook command
- `vars/vars.yml` are the externall defined varialbles
  ```
  ---
  - hosts: all
    become: true

    vars_files:
    - vars/vars.yml
  ```
  > example of how to include variables from another file

---
`lineinfile` module : ensures a particular line of text exists or doesnt exist in a file.
  ```
  - name: adjust opcache memory setting
    lineinefile:
      dest: "/etc/php/7.4/apache2/conf.d/10-opcache.ini"
      regexp: "^opcache.memory_consumption"
      line: "opcache.memory.consumption = 96"
      state: present
    notify: restart apache
  ```
  - dest : provides the location of the file
  - regexp: provides regex to define what the line looks like (python style regex)
  - line: defines what the line should look like
  - state: determines whether line should be present or absent
  
## Handlers
---
- Example Handler Syntax
  ```
  handlers:
    - name: restart apache
      service: name=apache2 state=restarted

  tasks:
    - name: enable apache rewrite module
      apache2_module: name=rewrite state=present
      notify: restart apache
  ```

- Chaining Handlers
  - Handlers operate like tasks, therefore they can be chained together like so:
  ```
  handlers:
    - name: restart apache
      service: name=apache2 state=restarted
      notify: restart memcached

    - name: restart memcached
      service: name=memcached state=restarted
  ```

- Handlers only run if a tasks notifies the handler
- Handlers only run once and only once, to overwrite this behavior, use the `meta` module
- Handlers only run if the task is called and succeeds, this can be modified by utlizing the `--force-handlers` flag which will run all handlers.

## Environment Variables
---
- Setting environment variable for remote user, by adding it to `.bash_profile`
```
- name: add an environment variable to the remote users shell
  lineinfile:
    dest: ~/.bash_profile
    regexp: '^ENV_VAR'
    line: "ENV_VAR=value"
```

- It is recommended to store the env variable with `register` if intending to use the env variable for further tasks
  ```
  - name: get the env variable
    shell: 'source ~/.bash_profile && echo $ENV_VAR'
    register: foo

  - name: print value of the environment variable
    debug:
      msg: "the variable is {{ foo.stdout }}"
  ```

- Linux also reads env variables from /etc/environment
  ```
  - name: add global environment variable
    lineinfile:
      dest: /etc/environment
      regexp: '^ENV_VAR='
      line: "ENV_VAR=value"
    become: yes

  ```

- Environment Variable per Task
  ```
  - name: download a file using example-proxy
    get_url:
      url: http://www.example.com/file.tar.gz
      dest: ~/Downloads/
    environment:
      http_proxy: http://example-proxy:80/
  ```


## Variables
---
- Variables in ansible work similar to other systems
- Valid variable names : foo, foo_bar, foo_bar_5, _foo
- Prefixed underscores typically represent private variables although this is not enforced
  

## Playbook Variables
---
- passing in variables
```
ansible-playbook example.yml --extra-vars "foo=bar"
```

- passing in variables using JSON, YAML or JSON/YAML file
```
--extra-vars "@even_more_vars.json"
--extra-vars "@even_more_vars.yaml"
```

- Variables can be included in the playbook
  ```
  ---
  - hosts: example
  - vars:
    foo: bar
  ...
  ```
- Variables in a separate file
  ```
  ...
  vars_files:
    - vars.yml
  ...
  ```