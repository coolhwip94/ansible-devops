# Running Docker Container

Ansible can be utilized to run docker containers and even docker-compose.

> https://docs.ansible.com/ansible/latest/collections/community/docker/docker_container_module.html#ansible-collections-community-docker-docker-container-module


## Container Example
---
Files Associated : 
- group_vars/dockerhost.yml
  - this holds variables for dockerhost
  - group `dockerhost` in hosts file
  - playbooks/run_docker_container.yml


Installing community.docker.docker_container
```
ansible-galaxy collection install community.docker
```

Using it in playbook
```
community.docker.docker_container
```

- Running hello-world container
  ```yml
      - name: Run a docker container
        community.docker.docker_container:
          name: world-hello
          image: hello-world
  ```

- Running Nginx Container:
  ```yml
    - name: Run Nginx container
      community.docker.docker_container:
        name: hwip_nginx
        image: nginx
        ports:
          - 8080:80

    - name: enable port 8080
      command: ufw allow 8080
  ```
> Enabling port 8080 will allow you to visit reach that port on a particular host, `curl scripthost.hwip.lab:8080` to test without using a browser.

- Ad hoc stopping container
  ```
  ansible dockerhost -m community.docker.docker_container -a "name=hwip_nginx state=absent"
  ```

- Ad hoc starting container
  ```
  ansible dockerhost -m community.docker.docker_container -a "name=hwip_nginx state=started image=nginx ports=8080:80"
  ```



